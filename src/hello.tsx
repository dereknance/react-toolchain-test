import * as React from "react";
import { Wussup } from "./wussup";

export interface HelloProps { compiler: string; framework: string; }

export const Hello = (props: HelloProps) => <div><h1>Hello from {props.compiler} and {props.framework}!</h1><Wussup name="dude"/></div>;

